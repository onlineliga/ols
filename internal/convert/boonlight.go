package convert

import (
	"fmt"
	"strings"
)

func FloatToHumanReadable(value float64) string {
	return strings.Replace(fmt.Sprintf("%.2f", value), ".", ",", 1)
}

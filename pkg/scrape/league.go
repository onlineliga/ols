package scrape

import (
	"fmt"
	"net/http"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/onlineliga/ols/internal/request"
	"gitlab.com/onlineliga/ols/internal/response"
	"gitlab.com/onlineliga/ols/pkg/onlineliga"
)

func FetchLeagueDocument(league *onlineliga.League, season int) *goquery.Document {
	leagueOptions := &request.RequestOption{
		Path: "/matchdaytable/leaguetable",
		QueryParameters: map[string]string{
			"season":         fmt.Sprintf("%v", season),
			"matchday":       fmt.Sprintf("%v", 34),
			"leagueLevel":    fmt.Sprintf("%v", 1),
			"leagueId":       fmt.Sprintf("%v", league.ID),
			"leagueMatchday": fmt.Sprintf("%v", 1),
			"type":           fmt.Sprintf("%v", 1),
			"nav":            fmt.Sprintf("%v", true),
			"navId":          fmt.Sprintf("%v", "matchdayTable"),
		},
	}
	res := request.DoRequest(http.DefaultClient, request.GET(leagueOptions))
	doc := response.ResponseToDocument(res)
	return doc
}

package utils

import "log"

func SumAvgFloat64(array []float64) (float64, float64) {
	if len(array) == 0 {
		log.Println("empty array, nothing to calculate")
		return 0, 0
	}

	var sum = 0.0
	for _, number := range array {
		sum += number
	}
	return sum, sum / float64(len(array))
}

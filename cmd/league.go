/*
Copyright © 2021 tripleawwy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/onlineliga/ols/internal/boonlight"
	"gitlab.com/onlineliga/ols/internal/utils"
	"gitlab.com/onlineliga/ols/pkg/onlineliga"
	"gitlab.com/onlineliga/ols/pkg/scrape"
	"gitlab.com/onlineliga/ols/pkg/status"
)

var leagueID int

// leagueCmd represents the league command
var leagueCmd = &cobra.Command{
	Use:   "league",
	Short: "scraping all teams of a specified league",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		// validate season
		currentSeason := int(status.CurrentInformation.Season)
		if season == 0 || season > currentSeason+1 {
			season = currentSeason
		}

		// create league and scrape league document and team ids
		league := &onlineliga.League{
			ID: leagueID,
		}
		leagueDoc := scrape.FetchLeagueDocument(league, season)
		league.GetName(leagueDoc)
		league.GetTeams(leagueDoc)

		// fill teams with players
		for i := range league.Teams {
			team := &league.Teams[i]
			teamInfoDoc := scrape.FetchTeamDocument(team, "")
			teamSquadDoc := scrape.FetchTeamDocument(team, "/squad")
			team.GetName(teamInfoDoc)
			team.GetPlayers(teamSquadDoc)

			// fill players with life
			for i := range team.Players {
				player := &team.Players[i]
				playerDetailsDoc := scrape.FetchPlayerDocument(player, "/details")
				player.GetProfile(playerDetailsDoc)
			}
			boonlight.ToCSV(league.Name, team)
		}
		utils.ToJSONFile(league, league.Name, output)
	},
}

func init() {
	rootCmd.AddCommand(leagueCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// leagueCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	leagueCmd.Flags().IntVar(&leagueID, "id", 1, "league id")
}

<!-- Powershell-->
## Powershell Examples

Some simple animated piccture to visualize usage with Powershell
### Installation
Remove Windows alias for cURL and create a directory in your home path
```powershell
remove-item alias:curl
mkdir $HOME\ols\
```
Download the latest executable with cURL
```powershell
$JSON = curl -s https://gitlab.com/api/v4/projects/31154129/repository/tags | ConvertFrom-Json
$TAG = $JSON[0].name
curl -o $HOME\ols\ols.exe -L https://gitlab.com/onlineliga/ols/-/jobs/artifacts/$TAG/raw/binaries/ols-windows-amd64.exe?job=build-release
```
![install][ps_install]

### Version validating and Help command
Add the executable directory to the `PATH` environment variable in order to be executed from anywhere.
```powershell
$env:Path += ";$HOME\ols\"
```
Execute `ols` with the `help` flag which shows available subcommands and flags
```powershell
ols --help
```
Execute `ols` with the `version` flag to show the version you currently are using. Ensure you're [up to date][releases].
```powershell
ols --version
```
![help][ps_help]

### Scrape a league
Scrape Germany's league with the ID `1`
```powershell
ols league --id 1
```
![league][ps_league]

<!-- IMAGES and LINKS -->
[releases]: https://gitlab.com/onlineliga/ols/-/releases

[ps_league]: ../img/ols_league.gif
[ps_install]: ../img/ols_install.gif
[ps_help]: ../img/ols_help.gif

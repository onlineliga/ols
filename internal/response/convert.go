package response

import (
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func ResponseToByte(res *http.Response) []byte {
	defer CloseBody(res)

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	//debug logs
	// log.Println(body)

	return body
}

func ResponseToString(res *http.Response) string {

	bodyAsString := string(ResponseToByte(res))

	//debug logs
	// log.Println(bodyAsString)

	return bodyAsString
}

func ResponseToDocument(res *http.Response) *goquery.Document {
	body := ResponseToString(res)

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(body))
	if err != nil {
		log.Fatal(err)
	}
	return doc
}

func StringToDocument(body string) *goquery.Document {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(body))
	if err != nil {
		log.Fatal(err)
	}
	return doc
}

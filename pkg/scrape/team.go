package scrape

import (
	"fmt"
	"net/http"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/onlineliga/ols/internal/request"
	"gitlab.com/onlineliga/ols/internal/response"
	"gitlab.com/onlineliga/ols/pkg/onlineliga"
)

func FetchTeamDocument(team *onlineliga.Team, navigation string) *goquery.Document {
	teamSquadOptions := &request.RequestOption{
		Path: fmt.Sprintf("/team/overview%s", navigation),
		QueryParameters: map[string]string{
			"userId": fmt.Sprintf("%v", team.ID),
		},
	}
	res := request.DoRequest(http.DefaultClient, request.GET(teamSquadOptions))
	doc := response.ResponseToDocument(res)
	return doc
}

package onlineliga

import (
	"log"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type League struct {
	ID    int    `json:"league_id"`
	Name  string `json:"league_name"`
	Teams []Team `json:"teams"`
}

func (league *League) GetName(leagueDocument *goquery.Document) {
	selection := leagueDocument.Find(".ol-matchday-table-header2")
	value := selection.Children().First().Text()
	league.Name = strings.TrimSpace(value)
}

func (league *League) GetTeams(leagueDocument *goquery.Document) {
	teams := teamIDs(leagueDocument)

	for _, teamID := range teams {
		team := &Team{
			ID: teamID,
		}
		league.Teams = append(league.Teams, *team)
	}

	//debug logs
	// log.Println(league.Teams)
}

func teamIDs(leagueDocument *goquery.Document) []int {
	var teamIDs []int
	selection := leagueDocument.Find(".text-left.bold .ol-team-name")
	selection.Each(func(i int, s *goquery.Selection) {
		anchor := s.AttrOr("onclick", "nope")
		teamIDs = append(teamIDs, parseUserId(anchor))
	})

	//debug logs
	// log.Printf("team IDs: %v", teamIDs)

	return teamIDs
}

func parseUserId(htmlstring string) int {
	splitted := strings.SplitAfter(htmlstring, "userId : ")
	trimmed := strings.TrimRight(splitted[len(splitted)-1], " });")
	id, err := strconv.ParseInt(trimmed, 0, 64)
	if err != nil {
		log.Println(err, "no user ID found")
		return 0
	}
	return int(id)
}

package boonlight

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"

	"gitlab.com/onlineliga/ols/internal/convert"
	"gitlab.com/onlineliga/ols/internal/utils"
	"gitlab.com/onlineliga/ols/pkg/onlineliga"
	"gitlab.com/onlineliga/ols/pkg/status"
)

func ToCSV(leagueName string, team *onlineliga.Team) {
	csvFile, err := os.OpenFile("./data.csv", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		csvFile, err = os.Create("./data.csv")
	}

	if err != nil {
		log.Println(err)
	}
	defer csvFile.Close()

	writer := csv.NewWriter(csvFile)
	writer.Comma = '\t'
	writer.Write(SPrint(leagueName, team))
	writer.Flush()
}

func SPrint(leagueName string, team *onlineliga.Team) []string {

	mvSum, mvAvg := team.MarketValue()
	salariesSum, salariesAvg := team.Salary()
	_, ageAvg := team.Age()
	expiringContracts := team.ExpiringContracts()

	top15 := team.Top15Players()
	_, t15TalentAvg := talent(top15)
	_, t15AgeAvg := age(top15)
	_, t15OverallAvg := overall(top15)

	tw := team.PlayersByPosition([]string{"Torwart", "Goalkeeper"})
	av := team.PlayersByPosition([]string{"Außenverteidiger", "Aussenverteidiger", "Full Back"})
	iv := team.PlayersByPosition([]string{"Innenverteidiger", "Centre Back"})
	dm := team.PlayersByPosition([]string{"Defensives Mittelfeld", "Defensive Midfield"})
	om := team.PlayersByPosition([]string{"Offensives Mittelfeld", "Attacking Midfield"})
	st := team.PlayersByPosition([]string{"Sturm", "Forward"})

	top2tw := tw
	if len(tw) > 2 {
		top2tw = tw[:2]
	}

	_, twAvg := overall(tw)
	_, top2twAvg := overall(top2tw)
	_, avAvg := overall(av)
	_, ivAvg := overall(iv)
	_, dmAvg := overall(dm)
	_, omAvg := overall(om)
	_, stAvg := overall(st)

	//debug logs
	log.Println(
		team.Name,
		strconv.Itoa(int(mvSum)),
		convert.FloatToHumanReadable(mvAvg),
		strconv.Itoa(int(salariesSum)),
		convert.FloatToHumanReadable(salariesAvg),
		strconv.Itoa(len(team.Players)),
		convert.FloatToHumanReadable(ageAvg),
		convert.FloatToHumanReadable(twAvg),
		convert.FloatToHumanReadable(avAvg),
		convert.FloatToHumanReadable(ivAvg),
		convert.FloatToHumanReadable(dmAvg),
		convert.FloatToHumanReadable(omAvg),
		convert.FloatToHumanReadable(stAvg),
		convert.FloatToHumanReadable(top2twAvg),
		convert.FloatToHumanReadable(t15OverallAvg),
		convert.FloatToHumanReadable(t15TalentAvg),
		convert.FloatToHumanReadable(t15AgeAvg),
		strconv.Itoa(expiringContracts),
		fmt.Sprintf("%s S%v W%v", leagueName, status.CurrentInformation.Season, status.CurrentInformation.Week),
	)

	return []string{
		team.Name,
		strconv.Itoa(int(mvSum)),
		convert.FloatToHumanReadable(mvAvg),
		strconv.Itoa(int(salariesSum)),
		convert.FloatToHumanReadable(salariesAvg),
		strconv.Itoa(len(team.Players)),
		convert.FloatToHumanReadable(ageAvg),
		convert.FloatToHumanReadable(twAvg),
		convert.FloatToHumanReadable(avAvg),
		convert.FloatToHumanReadable(ivAvg),
		convert.FloatToHumanReadable(dmAvg),
		convert.FloatToHumanReadable(omAvg),
		convert.FloatToHumanReadable(stAvg),
		convert.FloatToHumanReadable(top2twAvg),
		convert.FloatToHumanReadable(t15OverallAvg),
		convert.FloatToHumanReadable(t15TalentAvg),
		convert.FloatToHumanReadable(t15AgeAvg),
		strconv.Itoa(expiringContracts),
		fmt.Sprintf("%s S%v W%v", leagueName, status.CurrentInformation.Season, status.CurrentInformation.Week),
	}
}

func talent(players []onlineliga.Player) (float64, float64) {
	var values []float64
	for _, player := range players {
		values = append(values, player.Talent)
	}
	return utils.SumAvgFloat64(values)
}

func overall(players []onlineliga.Player) (float64, float64) {
	var values []float64
	for _, player := range players {
		values = append(values, player.Overall)
	}
	return utils.SumAvgFloat64(values)
}

func age(players []onlineliga.Player) (float64, float64) {
	var values []float64
	for _, player := range players {
		values = append(values, player.Age())
	}
	return utils.SumAvgFloat64(values)
}

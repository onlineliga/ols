package request

import (
	"log"
	"net/http"
	"net/url"
)

func GET(options *RequestOption) *http.Request {
	req := defaultGET()

	addQueryParams(req.URL, options.QueryParameters)
	addPath(req.URL, options.Path)

	//debug logs
	// log.Println(req.URL.RawQuery)
	// log.Println(req.URL.RequestURI())
	log.Println(req.URL)

	return req
}

func addPath(url *url.URL, path string) {
	url.Path = path
}

func addQueryParams(url *url.URL, queryParams map[string]string) {
	queries := url.Query()

	for key, value := range queryParams {
		queries.Add(key, value)
	}
	url.RawQuery = queries.Encode()
}

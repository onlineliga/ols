package request

import (
	"fmt"
	"log"
	"net/http"
)

type Endpoint struct {
	SubDomain      string
	TopLevelDomain string
	Scheme         string
}

var DefaultEndpoint = &Endpoint{
	SubDomain:      "www.onlineliga",
	TopLevelDomain: "de",
	Scheme:         "https",
}

func baseURL() string {
	return fmt.Sprintf(
		"%s://%s.%s",
		DefaultEndpoint.Scheme,
		DefaultEndpoint.SubDomain,
		DefaultEndpoint.TopLevelDomain,
	)
}

func defaultGET() *http.Request {
	req, err := http.NewRequest("GET", baseURL(), nil)
	if err != nil {
		log.Fatal(err)
	}
	return req
}

func DoRequest(client *http.Client, req *http.Request) *http.Response {
	res, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	return res
}

/*
Copyright © 2021 tripleawwy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/onlineliga/ols/internal/boonlight"
	"gitlab.com/onlineliga/ols/internal/utils"
	"gitlab.com/onlineliga/ols/pkg/onlineliga"
	"gitlab.com/onlineliga/ols/pkg/scrape"
)

var teamID int

// teamCmd represents the team command
var teamCmd = &cobra.Command{
	Use:   "team",
	Short: "scraping a single team by ID",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		team := &onlineliga.Team{
			ID: teamID,
		}
		teamInfoDoc := scrape.FetchTeamDocument(team, "")
		teamSquadDoc := scrape.FetchTeamDocument(team, "/squad")
		team.GetName(teamInfoDoc)
		team.GetPlayers(teamSquadDoc)

		// fill players with life
		for i := range team.Players {
			player := &team.Players[i]
			playerDetailsDoc := scrape.FetchPlayerDocument(player, "/details")
			player.GetProfile(playerDetailsDoc)
		}
		boonlight.ToCSV("", team)
		utils.ToJSONFile(team, team.Name, output)
	},
}

func init() {
	rootCmd.AddCommand(teamCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// teamCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// teamCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	teamCmd.Flags().IntVar(&teamID, "id", 0, "team id")
}

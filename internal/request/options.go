package request

type RequestOption struct {
	Path            string
	QueryParameters map[string]string
}

var DefaultOptions = &RequestOption{
	Path:            "",
	QueryParameters: make(map[string]string),
}

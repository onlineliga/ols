package onlineliga

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/onlineliga/ols/internal/convert"
	"gitlab.com/onlineliga/ols/internal/response"
	"gitlab.com/onlineliga/ols/internal/utils"
)

type Match struct {
	ID          int         `json:"match_id"`
	Season      int         `json:"season"`
	Matchday    int         `json:"matchday"`
	Attendance  int         `json:"attendance"`
	Competition string      `json:"competition"`
	Date        time.Time   `json:"date"`
	LineUps     []LineUp    `json:"line_ups"`
	Statistics  []Statistic `json:"statistics"`
}

type Goal struct {
	Minute int    `json:"minute"`
	Type   string `json:"type"`
	Scorer int    `json:"scorer"`
	Assist int    `json:"assist"`
}

type Booking struct {
	PlayerID int    `json:"player_id"`
	Colour   string `json:"colour"`
	Minute   int    `json:"minute"`
}

type Substitution struct {
	PlayerInID  int    `json:"player_in_id"`
	PlayerOutID int    `json:"player_out_id"`
	Minute      string `json:"minute"`
}

type LineUp struct {
	TeamID    int            `json:"team_id"`
	Team      string         `json:"team_name"`
	Formation string         `json:"formation"`
	Squad     map[string]int `json:"squad"`
}

type Statistic struct {
	TeamID        int            `json:"team_id"`
	Team          string         `json:"team_name"`
	Possession    int            `json:"ball_possession"`
	Attempts      int            `json:"goal_attempts"`
	Tackles       int            `json:"tackles"`
	Corners       int            `json:"corners"`
	Goals         []Goal         `json:"goals"`
	Bookings      []Booking      `json:"bookings"`
	Substitutions []Substitution `json:"substitution"`
}

func (match *Match) FetchStatistic(matchDoc *goquery.Document) {

	// // possession, tackles, goal attempts, corners
	// basicHome := match.fetchBasicStats(matchDoc, true)
	// basicAway := match.fetchBasicStats(matchDoc, false)

	// // debug logs
	// log.Println(basicHome)
	// log.Println(basicAway)

	fetchBookings(matchDoc, true)
	fetchBookings(matchDoc, false)

	fetchGoals(matchDoc, false)
	fetchGoals(matchDoc, true)
}

// possession, tackles, goal attempts, corners
func (match *Match) fetchBasicStats(matchDoc *goquery.Document, homeTeam bool) []string {
	selector := ".ol-match-statistic-value-big:not(.ol-match-statistic-value-balance):not(.ol-match-statistic-value-season-balance)"

	var content []string

	selection := matchDoc.Find(selector)

	if homeTeam {
		selection.Each(func(i int, s *goquery.Selection) {
			content = append(content, strings.TrimSpace(s.Children().First().Text()))
		})
	} else {
		selection.Each(func(i int, s *goquery.Selection) {
			content = append(content, strings.TrimSpace(s.Children().Last().Text()))
		})
	}
	return content
}

// returns goals
func fetchGoals(doc *goquery.Document, homeTeam bool) []Goal {
	// initialize empty goal slice to avoid nil while marshalling to JSON
	var goals []Goal = make([]Goal, 0)

	penalties := fetchPenalties(doc)

	var selector string
	if homeTeam {
		selector = ".ol-match-statistic-goals td:first-child"
	} else {
		selector = ".ol-match-statistic-goals td:last-child"
	}

	selection := doc.Find(selector)

	selection.Each(func(i int, s *goquery.Selection) {
		bookings := s.Find(".ol-match-statistic-item")
		if len(bookings.Nodes) == 0 && len(s.Children().Nodes) > 0 {
			// log.Println(len(s.Children().Nodes))

			var scorerID int
			var assistID int
			var goalType string
			var goalMinute int

			goal := Goal{}

			goalSelection := s.Find("span .ol-player-name")
			goalSelection.Each(func(i int, s *goquery.Selection) {
				if i == 0 {
					attribute, exist := s.Attr("onclick")
					if exist {
						scorerID = parsePlayerID(attribute)
						// debug logs
						// log.Printf("player ID: %v", scorerID)
					}
				} else if i == 1 {
					attribute, exist := s.Attr("onclick")
					if exist {
						assistID = parsePlayerID(attribute)
						// debug logs
						// log.Printf("player ID: %v", assistID)
					}
				}
			})

			timeSelection := s.Find(".statistics-minute.hidden-lg")
			goalMinute = convert.StatisticValueToInt(timeSelection.Text(), "'")
			// debug logs
			// log.Println("minute: " + fmt.Sprintf("%v", goalMinute))

			goalType = fmt.Sprintf("type: " + "regular")
			for _, penalty := range penalties {
				if strings.Contains(penalty, fmt.Sprintf("%v", goalMinute)) {
					goalType = fmt.Sprintf("type: " + "penalty")
				}
				// debug logs
				// log.Println(goalType)
			}

			goal.Assist = assistID
			goal.Scorer = scorerID
			goal.Minute = goalMinute
			goal.Type = goalType
			goals = append(goals, goal)

			// debug logs
			// utils.JSONPrint(goal)
		}
	})

	// debug logs
	utils.JSONPrint(goals)

	return goals
}

func fetchPenalties(doc *goquery.Document) []string {
	var penalties []string
	selector := ".icon-lineup_icon_penalty"

	selection := doc.Find(selector)
	selection.Each(func(i int, s *goquery.Selection) {
		attribute, exist := s.Parent().Attr("data-content")
		if exist {
			penaltyDetails := response.StringToDocument(attribute).Text()
			penalties = append(penalties, penaltyDetails)
		}
	})

	// debug logs
	// log.Println(penalties)

	return penalties
}

// returns bookings
func fetchBookings(doc *goquery.Document, homeTeam bool) []Booking {
	var selector string
	if homeTeam {
		selector = ".ol-match-statistic-goals td:first-child"
	} else {
		selector = ".ol-match-statistic-goals td:last-child"
	}
	selection := doc.Find(selector)

	bookingSelection := selection.Find(`
	.ol-match-statistic-item > .ol-player-name,
	span > [class^='icon-icon_'],
	.ol-match-statistic-item + .statistics-minute + .hidden-lg
	`)

	var bookings []Booking

	bookingSelection.Each(func(i int, s *goquery.Selection) {
		index := i / 3
		if i%3 == 0 {
			bookings = append(bookings, Booking{})
		}

		// add player ID
		attribute, exist := s.Attr("onclick")
		playerID := 0
		if exist {
			playerID = parsePlayerID(attribute)
			// log.Printf("player ID: %v", parsePlayerID(attribute))
			bookings[index].PlayerID = playerID
		}

		// add minute
		if s.HasClass("statistics-minute") {
			// log.Println(convert.StatisticValueToInt(s.Text(), "'"))
			bookings[index].Minute = convert.StatisticValueToInt(s.Text(), "'")
		}

		// add colour
		if s.HasClass("icon-icon_yellowcard") || s.HasClass("icon-icon_yellowredcard") || s.HasClass("icon-icon_redcard") {
			attributeName, exists := s.Attr("class")
			if exists {
				bookings[index].Colour = convert.BookingToColour(attributeName)
			} else {
				log.Println("could not find booking colour")
				bookings[index].Colour = "unknown"
			}
			// log.Println(attributeName)
		}

	})
	utils.JSONPrint(bookings)
	return bookings
}

// BOOKINGS
// basic selector
// .ol-match-statistic-goals td:first-child

// granular selector

// player ID
// .ol-match-statistic-item > .ol-player-name
// booking colour
// span > [class^='icon-icon_']
// booking time
// .ol-match-statistic-item + .statistics-minute + .hidden-lg

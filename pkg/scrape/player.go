package scrape

import (
	"fmt"
	"net/http"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/onlineliga/ols/internal/request"
	"gitlab.com/onlineliga/ols/internal/response"
	"gitlab.com/onlineliga/ols/pkg/onlineliga"
)

func FetchPlayerDocument(player *onlineliga.Player, navigation string) *goquery.Document {
	playerOptions := &request.RequestOption{
		Path: fmt.Sprintf("/player%s", navigation),
		QueryParameters: map[string]string{
			"playerId": fmt.Sprintf("%v", player.ID),
		},
	}
	res := request.DoRequest(http.DefaultClient, request.GET(playerOptions))
	doc := response.ResponseToDocument(res)
	return doc
}
